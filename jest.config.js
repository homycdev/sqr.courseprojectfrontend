module.exports = {
	testPathIgnorePatterns: ["<rootDir>/cypress/", "cypress/", "cache/"],
	moduleNameMapper: {
		"\\.(css|sass)$": "identity-obj-proxy",
	},
}