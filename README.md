# [S21] Software Quality and Reliability: Course Project

[![pipeline status](https://gitlab.com/homycdev/sqr.courseprojectfrontend/badges/master/pipeline.svg)](https://gitlab.com/homycdev/sqr.courseprojectfrontend/-/commits/master)

[![coverage report](https://gitlab.com/homycdev/sqr.courseprojectfrontend/badges/master/coverage.svg)](https://gitlab.com/homycdev/sqr.courseprojectfrontend/-/commits/master)

[![sqr.courseprojectfrontend](https://img.shields.io/endpoint?url=https://dashboard.cypress.io/badge/simple/7kw9f7&style=flat&logo=cypress)](https://dashboard.cypress.io/projects/7kw9f7/runs)

## Merge Request Policy:

**Before you will start coding - make sure that you have created an *issue* in the following format:**
1. SQRCP-##: __TITLE__ , where ## is for the issue track number(starts from 0), and title for title obviously.
2. Description is optional, but it is recommended to have it. 
3. In created issue you will see `Create Merge Request` button with down arrow from the right sidem press this arrow.
4. Make sure that branch name is as following - `sqrcp-##-title` 
5. Make sure that you have tagged `Create merge request and branch`
6. Create Merge Request



