import React from 'react'
import { store } from './__data__/store'
import { Provider } from 'react-redux'
import { Dashboard } from './containers/dashboard'
import 'antd/dist/antd.css'

export default () => (
    <Provider store={store}>
        <Dashboard />
    </Provider>
)
