import axios from 'axios'

export const commonAxios = axios.create({
    headers: {
        'Content-Type': 'application/json',
    },
})

export const apiAxios = axios.create({
    headers: {
        'Content-Type': 'application/json',
        Authorization: '7462CA3526AD91B8A28E911D806DD3C354710E30-1615817994767-x3',
    },
})
