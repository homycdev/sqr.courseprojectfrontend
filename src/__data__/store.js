import { combineReducers } from 'redux'
import { configureStore } from '@reduxjs/toolkit'

import usersReducer from './slices/users'
import authReducer from './slices/auth'
import groupsReducer from './slices/groups'

export const store = configureStore({
    reducer: combineReducers({
        users: usersReducer,
        auth: authReducer,
        groups: groupsReducer,
    }),
    devTools: { name: 'sqr' },
})
