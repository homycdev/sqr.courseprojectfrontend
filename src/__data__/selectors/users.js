const getUsers = (state) => state.users

export const getUsersList = (state) => getUsers(state).users

export const getDogssList = (state) => getUsers(state).dogs

export const getLoading = (state) => getUsers(state).loading
