const getUsers = (state) => state.groups

export const getGroups = (state) => getUsers(state).groups

export const getNewGroup = (state) => getUsers(state).newGroup
