const getUsers = (state) => state.auth

export const getAuthToken = (state) => getUsers(state).token

export const getAuthLoading = (state) => getUsers(state).loading

export const getAuthError = (state) => getUsers(state).error
