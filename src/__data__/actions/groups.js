import { apiAxios } from '../../axios'
import {
    handleFetch,
    handleSuccess,
    handleError,
    handleJoinGroup,
    handleCreateGroup,
} from '../slices/groups'

const tryFetchOrDispatchError = (dispatch) => async (func) => {
    dispatch(handleFetch())
    try {
        await func()
    } catch (error) {
        dispatch(handleError(error.name))
    }
}
const baseApiUrl = 'http://sqr-stage-833259366.eu-central-1.elb.amazonaws.com'

export const fetchGroups = () => async (dispatch) => {
    await tryFetchOrDispatchError(dispatch)(async () => {
        const response = await apiAxios.get(baseApiUrl + '/groups/all')

        dispatch(handleSuccess(response.data))
    })
}

export const createGroup = (name, description, founderId) => async (dispatch) => {
    await tryFetchOrDispatchError(dispatch)(async () => {
        const response = await apiAxios.post(baseApiUrl + '/groups/create', {
            name,
            description,
            founderId,
        })

        dispatch(handleCreateGroup(response.data))
    })
}

export const addGroup = (groupID) => async (dispatch) => {
    await tryFetchOrDispatchError(dispatch)(async () => {
        const response = await apiAxios.post(baseApiUrl + `/users/3/joinGroup?groupId=${groupID}`)

        dispatch(handleJoinGroup(response.data))
    })
}
