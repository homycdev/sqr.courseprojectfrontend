import { commonAxios, apiAxios } from '../../axios'

import { handleFetch, handleSuccess, handleError } from '../slices/users'
const tryFetchOrDispatchError = (dispatch) => async (func) => {
    dispatch(handleFetch())
    try {
        await func()
    } catch (error) {
        dispatch(handleError(error.name))
    }
}
const baseApiUrl = 'http://sqr-stage-833259366.eu-central-1.elb.amazonaws.com'
const dogBaseApiUrl = 'https://dog.ceo/api/breed/hound/images'

export const fetchUsers = () => async (dispatch) => {
    await tryFetchOrDispatchError(dispatch)(async () => {
        const response = await apiAxios.get(baseApiUrl + '/users/all')
        const dogsResponse = await commonAxios.get(dogBaseApiUrl)
        const result = {
            api: response.data,
            dogApi: dogsResponse.data.message.slice(0, response.data.length),
        }
        dispatch(handleSuccess(result))
    })
}
