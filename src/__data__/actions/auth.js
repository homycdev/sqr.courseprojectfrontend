import { apiAxios } from '../../axios'

import { handleFetch, handleSuccess, handleError } from '../slices/auth'

const tryFetchOrDispatchError = (dispatch) => async (func) => {
    dispatch(handleFetch())
    try {
        await func()
    } catch (error) {
        console.log({ error })
        dispatch(handleError(error.response.data.message))
    }
}
const baseApiUrl = 'http://sqr-stage-833259366.eu-central-1.elb.amazonaws.com'

export const login = (email, password) => async (dispatch) => {
    const params = {
        email: email,
        password: password,
    }
    await tryFetchOrDispatchError(dispatch)(async () => {
        const response = await apiAxios.post(baseApiUrl + '/login', { ...params })
        dispatch(handleSuccess(response.data))
    })
}
