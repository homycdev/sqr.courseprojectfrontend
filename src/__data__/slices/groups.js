/* eslint-disable no-param-reassign */

import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    groups: null,
    error: null,
    loading: false,
    newGroup: null,
}

const groups = createSlice({
    name: 'groups',
    initialState,
    reducers: {
        handleFetch: (state) => {
            state.loading = true
            state.error = null
        },
        handleSuccess: (state, action) => {
            state.groups = action.payload
            state.loading = false
            state.error = null
        },
        handleJoinGroup: (state, action) => {
            state.loading = false
            state.error = action.payload ? null : 'error'
        },
        handleCreateGroup: (state, action) => {
            state.newGroup = action.payload
            state.loading = false
            state.error = action.payload ? null : 'error'
        },
        handleError: (state, action) => {
            state.loading = false
            state.error = action.payload
        },
    },
})

export const {
    handleFetch,
    handleSuccess,
    handleError,
    handleJoinGroup,
    handleCreateGroup,
} = groups.actions

export default groups.reducer
