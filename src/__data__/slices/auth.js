/* eslint-disable no-param-reassign */

import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    token: null,
    error: null,
    loading: false,
}

const auth = createSlice({
    name: 'auth',
    initialState,
    reducers: {
        handleFetch: (state) => {
            state.loading = true
            state.error = null
        },
        handleSuccess: (state, action) => {
            state.token = action.payload
            state.loading = false
            state.error = null
        },
        handleError: (state, action) => {
            state.loading = false
            state.error = action.payload
        },
    },
})

export const { handleFetch, handleSuccess, handleError } = auth.actions

export default auth.reducer
