/* eslint-disable no-param-reassign */

import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    users: null,
    dogs: null,
    error: null,
    loading: false,
}

const users = createSlice({
    name: 'users',
    initialState,
    reducers: {
        handleFetch: (state) => {
            state.loading = true
            state.error = null
        },
        handleSuccess: (state, action) => {
            state.users = action.payload.api
            state.dogs = action.payload.dogApi
            state.loading = false
            state.error = null
        },
        handleError: (state, action) => {
            state.loading = false
            state.error = action.payload
        },
    },
})

export const { handleFetch, handleSuccess, handleError } = users.actions

export default users.reducer
