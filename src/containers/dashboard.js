import React from 'react'
import { Route, Switch, BrowserRouter as Router, Redirect } from 'react-router-dom'
import { Main } from './main'
import { Auth } from './auth'

export const Dashboard = () => (
    <Router>
        <Switch>
            <Route exact path="/">
                <Main />
            </Route>
            <Route exact path="/auth">
                <Auth />
            </Route>
        </Switch>
    </Router>
)
