import React, { useEffect } from 'react'
import './auth.css'
import { Form, Input, Button, Checkbox } from 'antd'
import { UserOutlined, LockOutlined } from '@ant-design/icons'
import { useDispatch, useSelector } from 'react-redux'

import * as selectors from '../__data__/selectors'
import { login } from '../__data__/actions/auth'
import { Redirect } from 'react-router'

export const Auth = () => {
    const dispatch = useDispatch()
    const loading = useSelector(selectors.auth.getAuthLoading)
    const error = useSelector(selectors.auth.getAuthError)
    const token = useSelector(selectors.auth.getAuthToken)

    useEffect(() => {
        if (token) {
            localStorage.setItem('token', token)
        }
    }, [token])

    useEffect(() => {
        if (error) {
            alert(error)
        }
    }, [error])

    if (token) {
        return <Redirect to={'/'} />
    }
    if (loading) {
        return <p>Loading ...</p>
    }

    const onFinish = (values) => {
        dispatch(login(values.email, values.password))
    }

    return (
        <div className="wrapper">
            <Form
                name="normal_login"
                className="login-form"
                initialValues={{
                    remember: true,
                }}
                onFinish={onFinish}
            >
                <Form.Item
                    name="email"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your email!',
                        },
                    ]}
                >
                    <Input
                        prefix={<UserOutlined className="site-form-item-icon" />}
                        placeholder="Email"
                        id="email-input"
                    />
                </Form.Item>
                <Form.Item
                    name="password"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your Password!',
                        },
                    ]}
                >
                    <Input
                        prefix={<LockOutlined className="site-form-item-icon" />}
                        type="password"
                        id="password-input"
                        placeholder="Password"
                    />
                </Form.Item>
                <Form.Item>
                    <Form.Item name="remember" valuePropName="checked" noStyle>
                        <Checkbox>Remember me</Checkbox>
                    </Form.Item>

                    <a className="login-form-forgot" href="">
                        Forgot password
                    </a>
                </Form.Item>

                <Form.Item>
                    <Button
                        id="login"
                        type="primary"
                        htmlType="submit"
                        className="login-form-button"
                    >
                        Log in
                    </Button>
                    Or <a href="">register now!</a>
                </Form.Item>
            </Form>
        </div>
    )
}
