import React from 'react'
import { mount, configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import mockAdapter from 'axios-mock-adapter'
import { act } from 'react-dom/test-utils'

import { commonAxios, apiAxios } from '../../axios'

import { Provider } from 'react-redux'

import { store } from '../../__data__/store'

import { Main } from '../main'

configure({ adapter: new Adapter() })

const dogs = [
    'https://images.dog.ceo/breeds/hound-afghan/n02088094_1003.jpg',
    'https://images.dog.ceo/breeds/hound-afghan/n02088094_1007.jpg',
    'https://images.dog.ceo/breeds/hound-afghan/n02088094_1023.jpg',
    'https://images.dog.ceo/breeds/hound-afghan/n02088094_10263.jpg',
]

const users = [
    {
        id: 4,
        email: 'homyc@gmail.com',
        firstname: null,
        lastname: null,
        topics: [],
        groups: [
            { id: 3, groupname: 'Group1', groupdesc: 'Пса сюда!', founderid: 4 },
            {
                id: 4,
                groupname: 'Group2',
                groupdesc: 'Rish is gay!',
                founderid: 4,
            },
        ],
    },
    { id: 3, email: 'rish@mail.com', firstname: null, lastname: null, topics: [], groups: [] },
    {
        id: 2,
        email: 'b@email.com',
        firstname: 'John',
        lastname: 'Doe',
        topics: [{ id: 3, name: 'knowledge' }],
        groups: [
            {
                id: 1,
                groupname: 'Martial arts',
                groupdesc: 'Description is redundant',
                founderid: 1,
            },
        ],
    },
    {
        id: 1,
        email: 'example@email.com',
        firstname: 'Ivan',
        lastname: 'Ivanov',
        topics: [
            { id: 1, name: 'adventure' },
            { id: 2, name: 'sport' },
            { id: 3, name: 'knowledge' },
        ],
        groups: [
            {
                id: 1,
                groupname: 'Martial arts',
                groupdesc: 'Description is redundant',
                founderid: 1,
            },
        ],
    },
]

const groups = [
    { id: 8, name: 'Really last one', description: '321', founderId: 3, topics: [] },
    {
        id: 7,
        name: 'Last one',
        description: '123',
        founderId: 3,
        topics: [],
    },
    { id: 6, name: 'Check', description: 'Check', founderId: 3, topics: [] },
    {
        id: 5,
        name: 'New group',
        description: 'Check',
        founderId: 3,
        topics: [],
    },
    { id: 4, name: 'Group2', description: 'Rish is gay!', founderId: 4, topics: [] },
    {
        id: 3,
        name: 'Group1',
        description: 'Пса сюда!',
        founderId: 4,
        topics: [],
    },
    {
        id: 2,
        name: 'Scala',
        description: 'newbie => pro',
        founderId: 2,
        topics: ['knowledge', 'science', 'books'],
    },
    {
        id: 1,
        name: 'Martial arts',
        description: 'Description is redundant',
        founderId: 1,
        topics: ['adventure', 'sport', 'knowledge'],
    },
]

const baseApiUrl = 'http://sqr-stage-833259366.eu-central-1.elb.amazonaws.com'
const dogBaseApiUrl = 'https://dog.ceo/api/breed/hound/images'

const multipleRequest = async (mock, responses) => {
    await act(async () => {
        await mock.onAny().reply((config) => {
            const [method, url, params, ...response] = responses.shift()
            if (config.url.includes(url)) {
                return response
            }
        })
    })
}

describe('Тестирование всего приложения', () => {
    let commonMockAxios
    let apiMockAxios
    beforeEach(() => {
        commonMockAxios = new mockAdapter(commonAxios)
        apiMockAxios = new mockAdapter(apiAxios)
    })
    beforeAll(() => {
        Object.defineProperty(window, 'matchMedia', {
            writable: true,
            value: jest.fn().mockImplementation((query) => ({
                matches: false,
                media: query,
                onchange: null,
                addListener: jest.fn(), // Deprecated
                removeListener: jest.fn(), // Deprecated
                addEventListener: jest.fn(),
                removeEventListener: jest.fn(),
                dispatchEvent: jest.fn(),
            })),
        })
    })
    it('Рендер', async () => {
        const component = mount(
            <Provider store={store}>
                <Main />
            </Provider>
        )

        expect(component).toMatchSnapshot()

        const response = [
            [
                'GET',
                dogBaseApiUrl,
                {},
                200,
                {
                    message: dogs,
                },
            ],
        ]

        const apiResponse = [
            ['GET', baseApiUrl + '/users/all', {}, 200, users],
            ['GET', baseApiUrl + '/groups/all', {}, 200, groups],
        ]

        await act(async () => {
            await multipleRequest(apiMockAxios, apiResponse)
            await multipleRequest(commonMockAxios, response)
        })

        component.update()

        expect(component).toMatchSnapshot()
    })
})
