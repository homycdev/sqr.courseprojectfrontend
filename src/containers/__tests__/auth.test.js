import React from 'react'
import { mount, configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import mockAdapter from 'axios-mock-adapter'
import { act } from 'react-dom/test-utils'

import { apiAxios } from '../../axios'

import { Provider } from 'react-redux'

import { store } from '../../__data__/store'

import { Main } from '../main'
import { Auth } from '../auth'

configure({ adapter: new Adapter() })

const baseApiUrl = 'http://sqr-stage-833259366.eu-central-1.elb.amazonaws.com'

const multipleRequest = async (mock, responses) => {
    await act(async () => {
        await mock.onAny().reply((config) => {
            const [method, url, params, ...response] = responses.shift()
            if (config.url.includes(url)) {
                return response
            }
        })
    })
}

describe('Тестирование всего приложения', () => {
    let apiMockAxios
    beforeEach(() => {
        apiMockAxios = new mockAdapter(apiAxios)
    })
    beforeAll(() => {
        Object.defineProperty(window, 'matchMedia', {
            writable: true,
            value: jest.fn().mockImplementation((query) => ({
                matches: false,
                media: query,
                onchange: null,
                addListener: jest.fn(), // Deprecated
                removeListener: jest.fn(), // Deprecated
                addEventListener: jest.fn(),
                removeEventListener: jest.fn(),
                dispatchEvent: jest.fn(),
            })),
        })
    })
    it('Рендер auth', async () => {
        const component = mount(
            <Provider store={store}>
                <Auth />
            </Provider>
        )

        expect(component).toMatchSnapshot()

        component.find('input#email-input').simulate('change', {
            target: {
                value: 'test',
            },
        })

        component.find('input#password-input').simulate('change', {
            target: {
                value: 'test',
            },
        })
        component.update()

        expect(component).toMatchSnapshot()

        // логинимся с введенными полями
        component.find('form').simulate('submit')
        component.update()

        expect(component).toMatchSnapshot()

        const response = [
            [
                'POST',
                baseApiUrl + '/login',
                {},
                200,
                {
                    message: 'You are logged in',
                },
            ],
        ]
        // перехватываем запрос
        await multipleRequest(apiMockAxios, response)
        component.update()

        expect(component).toMatchSnapshot()
    })
})
