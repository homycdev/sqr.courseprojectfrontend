/* eslint-disable no-unreachable */

import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Tabs, List, Avatar, Button, Modal, Input } from 'antd'
import { batch } from 'react-redux'

import * as selectors from '../__data__/selectors'
import { fetchUsers } from '../__data__/actions/users'
import { addGroup, createGroup, fetchGroups } from '../__data__/actions/groups'

const { TabPane } = Tabs

function callback(key) {
    console.log(key)
}

export const Main = () => {
    const dispatch = useDispatch()
    const users = useSelector(selectors.users.getUsersList)
    const groups = useSelector(selectors.groups.getGroups)
    const dogs = useSelector(selectors.users.getDogssList)
    const loading = useSelector(selectors.users.getLoading)
    const newGroup = useSelector(selectors.groups.getNewGroup)
    // const error = useSelector(selectors.users.getError)
    const [showModal, setShowModal] = useState(false)
    const [showGroupModal, setShowGroupModal] = useState(false)
    const [groupName, setGroupName] = useState(null)
    const [groupDesc, setGroupDesc] = useState(null)
    const [id, setId] = useState(null)
    useEffect(() => {
        batch(() => {
            dispatch(fetchUsers())
            dispatch(fetchGroups())
        })
    }, [])

    useEffect(() => {
        if (groups) {
            dispatch(fetchGroups())
        }
    }, [newGroup])
    if (loading || !dogs || !groups) {
        return <p>Loading ...</p>
    }
    const handleGroupOk = (event) => {
        event.preventDefault()
        if (groupName && groupDesc) {
            dispatch(createGroup(groupName, groupDesc, 3))
            setShowGroupModal(false)
        }
    }

    const handleCancel = () => {
        setShowGroupModal(false)
        setShowModal(false)
    }

    const handleGroupCancel = () => {
        setShowModal(false)
    }

    return (
        <Tabs id="tabs" defaultActiveKey="1" onChange={callback}>
            <TabPane tab="Users" key="1">
                <>
                    <List
                        itemLayout="horizontal"
                        dataSource={users}
                        renderItem={(user, index) => (
                            <List.Item>
                                <List.Item.Meta
                                    avatar={<Avatar src={dogs[index]} />}
                                    title={<p>{user.firstname + ' ' + user.lastname}</p>}
                                    description="Ant Design, a design language for background applications, is refined by Ant UED Team"
                                />
                            </List.Item>
                        )}
                    />
                </>
            </TabPane>
            <TabPane tab="Groups" key="2">
                <>
                    <List
                        itemLayout="horizontal"
                        dataSource={groups}
                        renderItem={(group) => (
                            <List.Item
                                onClick={() => {
                                    setShowModal(true)
                                    setId(group.id)
                                }}
                            >
                                <List.Item.Meta
                                    title={<p>{group.name}</p>}
                                    description={<p>{group.description}</p>}
                                />
                            </List.Item>
                        )}
                    />
                    <Modal
                        title="Join group"
                        visible={showModal}
                        onOk={(e) => {
                            e.preventDefault()
                            dispatch(addGroup(id))
                            setShowModal(false)
                        }}
                        onCancel={handleCancel}
                    >
                        <p>Are you sure?</p>
                    </Modal>
                    <Modal
                        title="Create group"
                        visible={showGroupModal}
                        onOk={handleGroupOk}
                        onCancel={handleGroupCancel}
                    >
                        <Input
                            id="group-name-input"
                            placeholder="Name"
                            onChange={(e) => setGroupName(e.target.value)}
                        />
                        <Input
                            id="group-desc-input"
                            placeholder="Description"
                            onChange={(e) => setGroupDesc(e.target.value)}
                        />
                    </Modal>
                    <Button id="add-group" onClick={() => setShowGroupModal(true)}>
                        Add
                    </Button>
                </>
            </TabPane>
        </Tabs>
    )
}
