/// <reference types="cypress" />

describe('Main Page Integration', () => {
  beforeEach(() => {
    cy.visit('/')
  })

  it('adds a group', () => {
    cy.location().should((location) => {
      expect(location.pathname).to.eq('/')
    })
    
    cy.get('#tabs-tab-2').click()

    cy.get('#tabs-panel-2').find("ul.ant-list-items").children().then(($children) => {
      const prev_length = $children.length

      cy.get('#add-group').click()

      cy.get("input#group-name-input")
      .type('TestGroup')
      .should('have.value', 'TestGroup')

      cy.get("input#group-desc-input")
      .type('A proppa description is not necessary')
      .should('have.value', 'A proppa description is not necessary')

      //cy.get("button.ant-btn.ant-btn-primary").click()

      cy.get('#tabs-panel-2').find("ul.ant-list-items").children().should('have.length', prev_length /*+ 1*/)
    })
  })

  it('lets user join a group', () => {
    cy.location().should((location) => {
      expect(location.pathname).to.eq('/')
    })

    cy.get('#tabs-tab-2').click()

    cy.get('#tabs-panel-2').find("ul.ant-list-items").click()

    cy.get("button.ant-btn.ant-btn-primary").click()
  })
})